import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TestNewAuto {

    // test1
    @Test
    public void navigateToBBC() {

        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.bbc.com");

        WebElement searchField = driver.findElement(By.xpath("//input[@id='orb-search-q']"));
        searchField.clear();
        searchField.sendKeys("Test");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        WebElement searchButton = driver.findElement(By.xpath("//button[@id='orb-search-button']"));
        searchButton.click();

        WebElement mainButton = driver.findElement(By.xpath("//div[@class='orb-nav-section orb-nav-blocks']"));
        mainButton.click();

        WebElement firstLink = driver.findElement(By.xpath("//div[@class='media media--hero media--primary media--overlay block-link']/a"));
        firstLink.click();
    }
}
