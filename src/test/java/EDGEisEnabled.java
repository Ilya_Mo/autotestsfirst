import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class EDGEisEnabled {
    @Test
    public void edfeIsEnabled() {
        System.setProperty("webdriver.edge.driver", "C:\\MicrosoftWebDriver\\MicrosoftWebDriver.exe");
        WebDriver driver = new EdgeDriver();
        driver.get("https://www.bbc.com");

        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(By.xpath("//input[@id='orb-search-q']")).isEnabled();
            }
        });
    }
}
