import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class EDGEtest {
    @Test
    public void edfeBBC() {
        System.setProperty("webdriver.edge.driver", "C:\\MicrosoftWebDriver\\MicrosoftWebDriver.exe");
        WebDriver driver = new EdgeDriver();
        driver.get("https://www.bbc.com");

        WebElement searchField = driver.findElement(By.xpath("//input[@id='orb-search-q']"));
        searchField.clear();
        searchField.sendKeys("Test");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        WebElement searchButton = driver.findElement(By.xpath("//button[@id='orb-search-button']"));
        searchButton.click();


    }
}



